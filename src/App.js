import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Brush
} from 'recharts';

const data = [
  {
    date: '01/10/2020', euro: 68.0555, dollar: 61.2340, yuan: 8.8349
  },
  {
    date: '01/11/2020', euro: 68.045, dollar: 61.2632, yuan: 8.8397
  },
  {
    date: '01/14/2020', euro: 67.8162, dollar: 60.9474, yuan: 8.84
  },
  {
    date: '01/15/2020', euro: 68.4213, dollar: 61.4140, yuan: 8.9197
  },
  {
    date: '01/16/2020', euro: 68.3747, dollar: 61.4328, yuan: 8.9202
  },
  {
    date: '01/17/2020', euro: 68.656, dollar: 61.5694, yuan: 8.9421
  },
  {
    date: '01/18/2020', euro: 68.5358, dollar: 61.5333, yuan: 8.9710
  },
  {
    date: '01/21/2020', euro: 68.2082, dollar: 61.4654, yuan: 8.9586
  },
  {
    date: '01/22/2020', euro: 68.6222, dollar: 61.8552, yuan: 8.9551
  },
  {
    date: '01/23/2020', euro: 68.5186, dollar: 61.8343, yuan: 8.9583
  },
  {
    date: '01/24/2020', euro: 68.6856, dollar: 61.9515, yuan: 8.939
  },
  {
    date: '01/25/2020', euro: 68.2924, dollar: 61.8031, yuan: 8.9095
  },
  {
    date: '01/28/2020', euro: 68.7775, dollar: 62.3380, yuan: 8.9866
  },
  {
    date: '01/29/2020', euro: 69.226, dollar: 62.8299, yuan: 9.0576
  },
  {
    date: '01/30/2020', euro: 68.6764, dollar: 62.3934, yuan: 8.9946
  },
  {
    date: '01/31/2020', euro: 69.4151, dollar: 63.0359, yuan: 9.0873
  },
  {
    date: '02/01/2020', euro: 69.5976, dollar: 63.1385, yuan: 9.102
  },
  {
    date: '02/04/2020', euro: 70.7921, dollar: 63.9091, yuan: 9.0973
  },
  {
    date: '02/05/2020', euro: 70.1265, dollar: 63.4342, yuan: 9.0744
  },
  {
    date: '02/06/2020', euro: 69.7443, dollar: 63.1742, yuan: 9.0248
  },
  {
    date: '02/07/2020', euro: 69.0837, dollar: 62.7977, yuan: 9.0095
  },
  {
    date: '02/08/2020', euro: 69.6288, dollar: 63.4720, yuan: 9.0859
  },
];

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
     width:window.screen.width,
     left: window.screen.width >= 768 && window.screen.width <= 991 ? 0 : 20,
     right: window.screen.width >= 768 && window.screen.width <= 991 ? 0 : 30
    }
    this.changeWindow = this.changeWindow.bind(this);

  }
  
  componentDidMount() {
    window.addEventListener('resize', this.changeWindow);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.changeWindow);
  }
  changeWindow() {
    
    this.setState({
      
      left: window.screen.width >= 768 && window.screen.width <= 991 ? 0 : 20,
      right: window.screen.width >= 768 && window.screen.width <= 991 ? 0 : 50
    })
  }
  
  render() {
    
 /* weight(fixed) - 1100
    height - 600*/
 /* 1100 и 600 - значения ширины и высоты для одного из разрешений*/
  return (
    <div>
    <LineChart
        width={this.state.width * 0.6 }
        height={this.state.width * 0.6 *(6 / 11) }
        data={data}
        syncId="anyId"
        margin={{
          top: 5, right: this.state.right, left: this.state.left, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date"
         />
        <YAxis 
        domain={[55, 75]}/>
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="euro" stroke="#8444d8" activeDot={{ r: 4 }} />
        <Line type="monotone" dataKey="dollar" stroke="#8884d8" activeDot={{ r: 4 }} />
      </LineChart>
      <LineChart
      width={this.state.width *0.6}
      height={this.state.width * 0.6 * (6 / 11)}
      data={data}
      syncId="anyId"
      margin={{
        top: 5, right: this.state.right, left: this.state.left, bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="date" />
      <YAxis 
      domain={[8.5, 9.5]}/>
      <Tooltip />
      <Line type="monotone" dataKey="yuan" stroke="#82ca9d" activeDot={{ r: 4 }} />
    </LineChart>
    </div>
  );
      }
}

export default App;
