import React from 'react';

export default function MainComment(props) {
    return (
        <div class="main_comment">
            <h2>{props.name}</h2>
            <p>{props.comment}</p>
        </div>
    )
}