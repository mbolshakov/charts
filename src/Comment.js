import React from 'react';
import ButtonBlock from './ButtonBlock';
export default function Comment(props) {
    return(
        <div class="comment">
            <h3>{props.name}</h3>
            <p>{props.content}</p>
            <ButtonBlock likes={props.likes}/>
        </div>
    )
}