import React from 'react';

export default class ButtonBlock extends React.Component {
 constructor(props) {
     super(props);
     this.state = {
         likes: props.likes,
         liked: false
     }
     this.changeLikes = this.changeLikes.bind(this);
 }
 changeLikes() {
     if(!this.state.liked) {
     this.setState((state)=>({
        likes:state.likes + 1,
        liked:true
     })
     )
    }
    else {
        this.setState((state)=>({
            likes:state.likes - 1,
            liked:false
         })
         )
    }
 }
  render() {
    return(
        <div class="btnblock">
            
             <div class="btnLike" onClick={this.changeLikes}>Like</div>
             <p>Понравилось {this.state.likes}-пользователям(-ю)</p>
            
        </div>
    )
    }
}